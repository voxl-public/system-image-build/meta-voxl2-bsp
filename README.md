# meta-voxl2-bsp

## conf

### machine

| Machine  | Variant   | Description                         | modalai,platform-id (machine, variant, pad)|
|----------|-----------|-------------------------------------|---------------------|
| m0052    |           | RB5 Flight based SOM carrier        | 0 0 0               |
| m0054    |           | VOXL2                               | 1 0 0               |
| m0054    | var01     | VOXL2 - no combo mode J6/J8         | 1 1 0               |
| m0054    | var02     | VOXL2 - 8250                        | 1 2 0               |
| m0104    |           | VOXL2 Mini                          | 2 0 0               |

## recipes-android

### adb

This recipe changes default shell to bash (away from sh).

## recipes-kernel

### edk2

Patches a flag used in fastboot to prevent checking for battery voltage (EnableBatteryVoltageCheck)

### linux-msm

This is the primary recipe used to configure the device tree for targets above.

Look at the `linux-msm_4.19.bbappend` for usage.

The following environment variables are in play: `MACHINE` and `VARIANT`, see `files/dts` for MACHINE types and VARIANT types.

#### files/patches


- 006-subsystem-restart.patch - keep SLPI proc running if  APPS proc crashes
- 007-rt-spi.patch - enabled SPI controller RT flag by default, will run message pump with realtime priority
- 008-i2c-buffer-increase.patch - increase I2C write buffer size to support OTA of certain sensors
