# voxl-fsync Kernel Module

## Summary

This kernel module will create a pulse at a given sample period on a single GPIO (default GPIO 109).

Enable like this:

```
voxl2:/$ echo 1 > /sys/module/voxl_fsync_mod/parameters/enabled
```

A single client can then get timestamps like so:

```
voxl2:/$ cat /dev/voxl-fsync
765219760488
765253093144
765287345956
765319756894
```

## Usage

### Enable Sync Pulse

```
echo 1 /sys/module/voxl_fsync_mod/parameters/enabled
```

### Read Timestamps

```
cat /dev/voxl-fsync
```

### Change Sample Period

Sample Period = (1/FPS), units are nanoseconds

```
echo 33333333 > /sys/module/voxl_fsync_mod/parameters/sampling_period_ns
```

### Change GPIO to use

```
echo 109 > /sys/module/voxl_fsync_mod/parameters/gpio_num
```

### Change Pulse Width

Units are microseconds.

```
echo 10 > /sys/module/voxl_fsync_mod/parameters/pulse_width_us
```

## VOXL2 GPIO Info

The following GPIO are used by the `voxl-fsync-mod`, intented to be used on M0173 and newer supported kernels (var00.1, var02.1).

| GPIO  | QRB Default Usage | ModalAI Usage          | HW                    | Interposer |
|-------|-------------------|------------------------|-----------------------|------------|
| 109   | M0173 based sync  | voxl-fsync-mod enabled |                       |            |


These are intialized via this driver and device tree node `voxl_fsync` here:

```
meta-voxl2-bsp/recipes-kernel/linux-msm/files/dts/common/m0xxx-modalai-gpio.dtsi
```