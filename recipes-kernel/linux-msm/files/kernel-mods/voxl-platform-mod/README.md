# voxl-platform Kernel Module

## Summary

This kernel module will probe the device tree to indentify hardware type and export to:

```
/sys/module/voxl_platform_mod/parameters/machine
/sys/module/voxl_platform_mod/parameters/variant
/sys/module/voxl_platform_mod/parameters/config
```

| Target             | Machine   | Variant/Config   | Description                         | modalai,platform-id (machine, variant, config)|
|--------------------|-----------|-----------|-------------------------------------|---------------------|
| m0052              | 0         |           | RB5 Flight based SOM carrier        | 0 0 0               |
| ---| | | | |
| m0054-1/m0154-1    | 1         | var00.0   | M0X54-1 (5165)                      | 1 0 0               |
| m0054-1/m0154-1    | 1         | var00.1   | M0X54-1 (5165) with M0173           | 1 0 1               |
| --- | | | | |
| m0054-1/m0154-1    | 1         | var01.0   | M0X54-1 (5165) no combo mode J6/J8  | 1 1 0               |
| --- | | | | |
| m0054-2/m0154-2    | 1         | var02.0   | M0X54-2 (8250)                      | 1 2 0               |
| m0054-2/m0154-2    | 1         | var02.1   | M0X54-2 (8250) with M0173           | 1 2 1               |
| --- | | | | |
| m0104-1/m0204-1    | 2         | var00.0    | M0104-1 (5165)                     | 2 0 0               |
| --- | | | | |
| m0205-1            | 3         | var00.0    | M0205-1 (5165)                     | 3 0 0               |



## See Also

The device tree is configured to export the values above using the following:

```
meta-voxl2-bsp/recipes-kernel/linux-msm/files/dts/m0054/m0054-modalai-platform.dtsi
meta-voxl2-bsp/recipes-kernel/linux-msm/files/dts/m0104/m0104-modalai-platform.dtsi
meta-voxl2-bsp/recipes-kernel/linux-msm/files/dts/m0205/m0205-modalai-platform.dtsi

meta-voxl2-bsp/recipes-kernel/linux-msm/files/dts/variant/m0054/var02/m0054-modalai-platform.dtsi
```