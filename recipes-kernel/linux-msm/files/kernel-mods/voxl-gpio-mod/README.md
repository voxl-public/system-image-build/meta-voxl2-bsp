# voxl-gpio Kernel Module

## Summary

This kernel module will initialize GPIO and export to user space for use with voxl-gpio VOXL SDK utility.

See also:

```
meta-voxl2-bsp/recipes-kernel/linux-msm/files/dts/common/m0xxx-modalai-gpio.dtsi
```
