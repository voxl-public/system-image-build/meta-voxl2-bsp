#@TYPE: Machine
#@NAME: voxl2-mini
#@DESCRIPTION: Machine configuration for VOXL2 Mini based on QRB5165-RB5

require conf/machine/include/m0104.inc

require conf/machine/include/toolchain.conf

# Our customized MACHINE is based off qrb5165-rb5 machine, this must be included
MACHINEOVERRIDES =. "qrb5165-rb5:"

KERNEL_IMAGETYPE   = "Image.gz"
KERNEL_DTB_NAMES    = "vendor/qcom/m0104-kona-v2.1-iot-rb5.dtb"
KERNEL_BASE        = "0x80000000"
KERNEL_TAGS_OFFSET = "0x81900000"

# These use weak assignment but, even so the _qti-distro-debug indication doesn't seem to work
# These are assigned in the distro conf
KERNEL_CONFIG     ?= "vendor/kona-perf_defconfig"
KERNEL_CONFIG_qti-distro-debug ?= "vendor/kona_defconfig"

CONSOLE_PARAM     ?= ""
CONSOLE_PARAM_qti-distro-debug ?= "console=ttyMSM0,115200,n8"

KERNEL_CMD_PARAMS  = "noinitrd ${CONSOLE_PARAM} earlycon=msm_geni_serial,0xa90000 androidboot.hardware=qcom androidboot.console=ttyMSM0 androidboot.memcg=1 lpm_levels.sleep_disabled=1 video=vfb:640x400,bpp=32,memsize=3072000 msm_rtb.filter=0x237 service_locator.enable=1 androidboot.usbcontroller=a600000.dwc3 swiotlb=2048 loop.max_part=7 cgroup.memory=nokmem,nosocket reboot=panic_warm net.ifnames=0 usbcore.autosuspend=-1"

SERIAL_CONSOLE    ?= ""
SERIAL_CONSOLE_qti-distro-debug ?= "115200 ttyMSM0"

# bootloader configuration
EXTRA_IMAGEDEPENDS += "edk2"

PREFERRED_PROVIDER_virtual/libgles1 = "adreno"
PREFERRED_PROVIDER_virtual/libgles2 = "adreno"
PREFERRED_PROVIDER_virtual/egl      = "adreno"

# Conf with partition entries required for machine.
MACHINE_PARTITION_CONF = "qrb5165-rb5-partition.conf"

# filesystem configurations to be applied on image.
MACHINE_FSCONFIG_CONF = "qrb5165-fsconfig.conf"

# MACHINE_FEATURES ##
# ubwc:                 Machine supports UBWC hardware
# qti-wifi:             Machine supports QTI WiFi solution
# qti-audio:            Machine supports QTI audio solution
# qti-audio-encoder:    Machine supports QTI audio encoder
# qti-audio-cal:        Machine supports QTI audio calibration solution
# qti-adsp:             Machine supports QTI adsp solution
# qti-ab-boot:          Machine support a/b boot
# opencl:               Machine supports OpenCL driver
# qti-tflite-delegate:  Machine supports tensorflow lite hardware acceleration
# drm :                 Machine supports DRM display drivers
# dm-verity-bootloader: Machine supports bootloader-based dm-verity implementation
# WLAN soc is naples.
# qti-security:        Support QTI security solution
# qti-gst-ros2:        Machine supports QTI Gst Ros2 solution
# qti-fscrypt:         Machine supports FBE using fscrypt

MACHINE_FEATURES += "ubwc qti-wifi hasting qti-adsp qti-audio qti-audio-encoder qti-audio-cal qti-ab-boot qti-cdsp opencl qti-tflite-delegate qti-camera drm qti-npu qti-security dm-verity-bootloader qti-slpi qti-sensors qti-fscrypt"

# Pull in nHLOS firmware into rootfs and install nHLOS images
# in deploydir if NHLOS_ARCHIVE_PATH is set
MACHINE_EXTRA_RDEPENDS += "${@'qti-firmware' if ((d.getVar('NHLOS_ARCHIVE_PATH') or '') != '') else ''}"

# Supported prebuilt variants
PREBUILT_VARIANTS  = "HY11 HY22"

# inherit sdllvm class
INHERIT += "sdllvm"

# LLVM version to use
LLVM_VERSION = "8.0"

# Set sectool5 as preffered provider for signing
PREFERRED_PROVIDER_virtual/signtools-native = "sectool5-native"

# Sign abl and boot images
ENABLE_SIGNED_IMAGES = "1"
BL_PLATFORM     = "msmcobalt"
SOC_VERSION     = "0x6008"
SOC_HW_VERSION  = "0x60080100"
SIGNING_VERSION = "v3"
