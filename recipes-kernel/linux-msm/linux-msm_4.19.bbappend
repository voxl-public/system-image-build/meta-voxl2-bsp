# This recipe modifies the kernel build.

THISAPPENDFILESDIR := "${THISDIR}/files"
FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

# Include fragment and build list of to merge
# To add more config fragments:
#   - Add the fragment file to the files directory
#   - Add the file name to the SRC_URI, as shown above
#   - Add the file path to the CFG_FRAGMENTS, as shown below

SRC_URI += "file://fragments/m005x.cfg"
CFG_FRAGMENTS = "${WORKDIR}/fragments/m005x.cfg "

# All the in tree kernel modules
SRC_URI += "file://kernel-mods"

# Various kernel source patches
SRC_URI += "file://patches/001-uart-disable-autosuspend.patch"
SRC_URI += "file://patches/003-add-siera-usb-desc-qmi-wwan.patch"
SRC_URI += "file://patches/004-quectel-add-usb-desc-qmi-wwan.patch"
SRC_URI += "file://patches/004-quectel-remove-usb-desc.patch"
SRC_URI += "file://patches/004-quectel-qmi-wwan.patch"
SRC_URI += "file://patches/004-quectel-usb-wwan.patch"
SRC_URI += "file://patches/005-telit-option.patch"
SRC_URI += "file://patches/005-telit-qmi-wwan.patch"
SRC_URI += "file://patches/006-dma-unmapping.patch"
SRC_URI += "file://patches/006-subsystem-restart.patch"
SRC_URI += "file://patches/007-rt-spi.patch"
SRC_URI += "file://patches/008-i2c-buffer-increase.patch"
SRC_URI += "file://patches/Add-directory-for-building-voxl-kernel-mods.patch"

# Kernel flag configs
SRC_URI += "file://configs/m0052/kona_defconfig"
SRC_URI += "file://configs/m0052/kona-perf_defconfig"
SRC_URI += "file://configs/m0054/kona_defconfig"
SRC_URI += "file://configs/m0054/kona-perf_defconfig"
SRC_URI += "file://configs/m0104/kona_defconfig"
SRC_URI += "file://configs/m0104/kona-perf_defconfig"

# Device Tree Files
#  common
SRC_URI += "file://dts/Makefile"
SRC_URI += "file://dts/common/m0xxx-modalai-gpio.dtsi"
SRC_URI += "file://dts/common/m0xxx-modalai-camera.dtsi"
SRC_URI += "file://dts/common/m0xxx-kona-pinctrl.dtsi"

# Device Tree Files
#  variants
SRC_URI += "file://dts/variant/m0054/var00.1/m0xxx-modalai-gpio.dtsi"
SRC_URI += "file://dts/variant/m0054/var00.1/m0xxx-kona-pinctrl.dtsi"
SRC_URI += "file://dts/variant/m0054/var00.1/m0054-modalai-camera.dtsi"
SRC_URI += "file://dts/variant/m0054/var00.1/m0054-modalai-platform.dtsi"

SRC_URI += "file://dts/variant/m0054/var01/m0054-modalai-camera-overlay.dtsi"
SRC_URI += "file://dts/variant/m0054/var01/m0054-modalai-platform.dtsi"

SRC_URI += "file://dts/variant/m0054/var02/m0xxx-modalai-camera.dtsi"
SRC_URI += "file://dts/variant/m0054/var02/m0054-modalai-platform.dtsi"

SRC_URI += "file://dts/variant/m0054/var02.1/m0xxx-modalai-gpio.dtsi"
SRC_URI += "file://dts/variant/m0054/var02.1/m0xxx-kona-pinctrl.dtsi"
SRC_URI += "file://dts/variant/m0054/var02.1/m0xxx-modalai-camera.dtsi"
SRC_URI += "file://dts/variant/m0054/var02.1/m0054-modalai-camera.dtsi"
SRC_URI += "file://dts/variant/m0054/var02.1/m0054-modalai-platform.dtsi"

SRC_URI += "file://dts/variant/m0104/var02/m0xxx-modalai-camera.dtsi"
SRC_URI += "file://dts/variant/m0104/var02/m0104-modalai-platform.dtsi"

SRC_URI += "file://dts/variant/m0104/var00.2/m0xxx-kona-pinctrl.dtsi"
SRC_URI += "file://dts/variant/m0104/var00.2/m0xxx-modalai-gpio.dtsi"
SRC_URI += "file://dts/variant/m0104/var00.2/m0104-modalai-camera.dtsi"
SRC_URI += "file://dts/variant/m0104/var00.2/m0104-modalai-platform.dtsi"

# Device Tree Files
## targets
SRC_URI += "file://dts/m0052/m0052-kona-qrd.dtsi"
SRC_URI += "file://dts/m0052/m0052-kona-qupv3.dtsi"
SRC_URI += "file://dts/m0052/m0052-kona-sde-display.dtsi"
SRC_URI += "file://dts/m0052/m0052-kona-v2.1-iot-rb5.dts"
SRC_URI += "file://dts/m0052/m0052-kona-v2.1-iot-rb5.dtsi"
SRC_URI += "file://dts/m0052/m0052-kona-v2.1.dtsi"
SRC_URI += "file://dts/m0052/m0052-kona-v2.dtsi"
SRC_URI += "file://dts/m0052/m0052-kona.dtsi"
SRC_URI += "file://dts/m0052/m0052-modalai-camera.dtsi"
SRC_URI += "file://dts/m0052/m0052-modalai-qupv3.dtsi"
SRC_URI += "file://dts/m0052/m0052-qrb5165.dtsi"

SRC_URI += "file://dts/m0054/m0054-kona-qrd.dtsi"
SRC_URI += "file://dts/m0054/m0054-kona-qupv3.dtsi"
SRC_URI += "file://dts/m0054/m0054-kona-sde-display.dtsi"
SRC_URI += "file://dts/m0054/m0054-kona-v2.1-iot-rb5.dts"
SRC_URI += "file://dts/m0054/m0054-kona-v2.1-iot-rb5.dtsi"
SRC_URI += "file://dts/m0054/m0054-kona-v2.1.dtsi"
SRC_URI += "file://dts/m0054/m0054-kona-v2.dtsi"
SRC_URI += "file://dts/m0054/m0054-kona.dtsi"
SRC_URI += "file://dts/m0054/m0054-modalai-camera.dtsi"
SRC_URI += "file://dts/m0054/m0054-modalai-platform.dtsi"
SRC_URI += "file://dts/m0054/m0054-modalai-camera-overlay.dtsi"
SRC_URI += "file://dts/m0054/m0054-modalai-qupv3.dtsi"
SRC_URI += "file://dts/m0054/m0054-qrb5165.dtsi"

SRC_URI += "file://dts/m0104/m0104-kona-qrd.dtsi"
SRC_URI += "file://dts/m0104/m0104-kona-qupv3.dtsi"
SRC_URI += "file://dts/m0104/m0104-kona-sde-display.dtsi"
SRC_URI += "file://dts/m0104/m0104-kona-v2.1-iot-rb5.dts"
SRC_URI += "file://dts/m0104/m0104-kona-v2.1-iot-rb5.dtsi"
SRC_URI += "file://dts/m0104/m0104-kona-v2.1.dtsi"
SRC_URI += "file://dts/m0104/m0104-kona-v2.dtsi"
SRC_URI += "file://dts/m0104/m0104-kona.dtsi"
SRC_URI += "file://dts/m0104/m0104-modalai-camera.dtsi"
SRC_URI += "file://dts/m0104/m0104-modalai-platform.dtsi"
SRC_URI += "file://dts/m0104/m0104-modalai-qupv3.dtsi"
SRC_URI += "file://dts/m0104/m0104-qrb5165.dtsi"

do_configure_append() {
   install -d ${S}/drivers/platform/voxl
   cp -r ${WORKDIR}/kernel-mods/* ${S}/drivers/platform/voxl/
}

do_patch_append() {
    bb.build.exec_func('do_update_machine', d)
    bb.build.exec_func('do_update_variant', d)
}

do_update_machine () {

   bbnote "ModalAI: updating Machine DTSI"

   # common
   cp ${WORKDIR}/dts/Makefile ${S}/arch/arm64/boot/dts/vendor/qcom/Makefile
   cp ${WORKDIR}/dts/common/m0xxx-modalai-gpio.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0xxx-modalai-gpio.dtsi
   cp ${WORKDIR}/dts/common/m0xxx-modalai-camera.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0xxx-modalai-camera.dtsi
   cp ${WORKDIR}/dts/common/m0xxx-kona-pinctrl.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0xxx-kona-pinctrl.dtsi

   if  [[ "${MACHINE}" == "m0052" ]]; then
      cp ${WORKDIR}/configs/m0052/kona_defconfig ${S}/arch/arm64/configs/vendor/kona_defconfig
      cp ${WORKDIR}/configs/m0052/kona-perf_defconfig ${S}/arch/arm64/configs/vendor/kona-perf_defconfig

      cp ${WORKDIR}/dts/m0052/m0052-kona-qrd.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0052-kona-qrd.dtsi
      cp ${WORKDIR}/dts/m0052/m0052-kona-qupv3.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0052-kona-qupv3.dtsi
      cp ${WORKDIR}/dts/m0052/m0052-kona-sde-display.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0052-kona-sde-display.dtsi
      cp ${WORKDIR}/dts/m0052/m0052-kona-v2.1-iot-rb5.dts ${S}/arch/arm64/boot/dts/vendor/qcom/m0052-kona-v2.1-iot-rb5.dts
      cp ${WORKDIR}/dts/m0052/m0052-kona-v2.1-iot-rb5.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0052-kona-v2.1-iot-rb5.dtsi
      cp ${WORKDIR}/dts/m0052/m0052-kona-v2.1.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0052-kona-v2.1.dtsi
      cp ${WORKDIR}/dts/m0052/m0052-kona-v2.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0052-kona-v2.dtsi
      cp ${WORKDIR}/dts/m0052/m0052-kona.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0052-kona.dtsi
      cp ${WORKDIR}/dts/m0052/m0052-modalai-camera.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0052-modalai-camera.dtsi
      cp ${WORKDIR}/dts/m0052/m0052-modalai-qupv3.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0052-modalai-qupv3.dtsi
      cp ${WORKDIR}/dts/m0052/m0052-qrb5165.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0052-qrb5165.dtsi

   elif  [[ "${MACHINE}" == "m0054" ]]; then
      cp ${WORKDIR}/configs/m0054/kona_defconfig ${S}/arch/arm64/configs/vendor/kona_defconfig
      cp ${WORKDIR}/configs/m0054/kona-perf_defconfig ${S}/arch/arm64/configs/vendor/kona-perf_defconfig

      cp ${WORKDIR}/dts/m0054/m0054-kona-qrd.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0054-kona-qrd.dtsi
      cp ${WORKDIR}/dts/m0054/m0054-kona-qupv3.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0054-kona-qupv3.dtsi
      cp ${WORKDIR}/dts/m0054/m0054-kona-sde-display.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0054-kona-sde-display.dtsi
      cp ${WORKDIR}/dts/m0054/m0054-kona-v2.1-iot-rb5.dts ${S}/arch/arm64/boot/dts/vendor/qcom/m0054-kona-v2.1-iot-rb5.dts
      cp ${WORKDIR}/dts/m0054/m0054-kona-v2.1-iot-rb5.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0054-kona-v2.1-iot-rb5.dtsi
      cp ${WORKDIR}/dts/m0054/m0054-kona-v2.1.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0054-kona-v2.1.dtsi
      cp ${WORKDIR}/dts/m0054/m0054-kona-v2.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0054-kona-v2.dtsi
      cp ${WORKDIR}/dts/m0054/m0054-kona.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0054-kona.dtsi
      cp ${WORKDIR}/dts/m0054/m0054-modalai-camera.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0054-modalai-camera.dtsi
      cp ${WORKDIR}/dts/m0054/m0054-modalai-platform.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0054-modalai-platform.dtsi
      cp ${WORKDIR}/dts/m0054/m0054-modalai-camera-overlay.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0054-modalai-camera-overlay.dtsi
      cp ${WORKDIR}/dts/m0054/m0054-modalai-qupv3.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0054-modalai-qupv3.dtsi
      cp ${WORKDIR}/dts/m0054/m0054-qrb5165.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0054-qrb5165.dtsi

    elif  [[ "${MACHINE}" == "m0104" ]]; then
      cp ${WORKDIR}/configs/m0104/kona_defconfig ${S}/arch/arm64/configs/vendor/kona_defconfig
      cp ${WORKDIR}/configs/m0104/kona-perf_defconfig ${S}/arch/arm64/configs/vendor/kona-perf_defconfig

      cp ${WORKDIR}/dts/m0104/m0104-kona-qrd.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0104-kona-qrd.dtsi
      cp ${WORKDIR}/dts/m0104/m0104-kona-qupv3.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0104-kona-qupv3.dtsi
      cp ${WORKDIR}/dts/m0104/m0104-kona-sde-display.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0104-kona-sde-display.dtsi
      cp ${WORKDIR}/dts/m0104/m0104-kona-v2.1-iot-rb5.dts ${S}/arch/arm64/boot/dts/vendor/qcom/m0104-kona-v2.1-iot-rb5.dts
      cp ${WORKDIR}/dts/m0104/m0104-kona-v2.1-iot-rb5.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0104-kona-v2.1-iot-rb5.dtsi
      cp ${WORKDIR}/dts/m0104/m0104-kona-v2.1.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0104-kona-v2.1.dtsi
      cp ${WORKDIR}/dts/m0104/m0104-kona-v2.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0104-kona-v2.dtsi
      cp ${WORKDIR}/dts/m0104/m0104-kona.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0104-kona.dtsi
      cp ${WORKDIR}/dts/m0104/m0104-modalai-camera.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0104-modalai-camera.dtsi
      cp ${WORKDIR}/dts/m0104/m0104-modalai-platform.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0104-modalai-platform.dtsi
      cp ${WORKDIR}/dts/m0104/m0104-modalai-qupv3.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0104-modalai-qupv3.dtsi
      cp ${WORKDIR}/dts/m0104/m0104-qrb5165.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0104-qrb5165.dtsi
   fi
}

do_update_variant () {

   if [[ "${MACHINE}" == "m0054" ]]; then
      if  [[ "${VARIANT}" == "var01" ]]; then
         bbnote "For machine: ${MACHINE} applying variant: ${VARIANT}"
         cp ${WORKDIR}/dts/variant/m0054/var01/m0054-modalai-platform.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0054-modalai-platform.dtsi
         cp ${WORKDIR}/dts/variant/m0054/var01/m0054-modalai-camera-overlay.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0054-modalai-camera-overlay.dtsi
      elif  [[ "${VARIANT}" == "var00.1" ]]; then
         bbnote "For machine: ${MACHINE} applying variant: ${VARIANT}"
         # temp...
         cp ${WORKDIR}/dts/variant/m0054/var00.1/m0xxx-kona-pinctrl.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0xxx-kona-pinctrl.dtsi
         cp ${WORKDIR}/dts/variant/m0054/var00.1/m0xxx-modalai-gpio.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0xxx-modalai-gpio.dtsi

         cp ${WORKDIR}/dts/variant/m0054/var00.1/m0054-modalai-camera.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0054-modalai-camera.dtsi
         cp ${WORKDIR}/dts/variant/m0054/var00.1/m0054-modalai-platform.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0054-modalai-platform.dtsi
      elif  [[ "${VARIANT}" == "var02" ]]; then
         bbnote "For machine: ${MACHINE} applying variant: ${VARIANT}"
         cp ${WORKDIR}/dts/variant/m0054/var02/m0054-modalai-platform.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0054-modalai-platform.dtsi
         cp ${WORKDIR}/dts/variant/m0054/var02/m0xxx-modalai-camera.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0xxx-modalai-camera.dtsi
      elif  [[ "${VARIANT}" == "var02.1" ]]; then
         bbnote "For machine: ${MACHINE} applying variant: ${VARIANT}"
         # temp...
         cp ${WORKDIR}/dts/variant/m0054/var02.1/m0xxx-kona-pinctrl.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0xxx-kona-pinctrl.dtsi
         cp ${WORKDIR}/dts/variant/m0054/var02.1/m0xxx-modalai-gpio.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0xxx-modalai-gpio.dtsi

         cp ${WORKDIR}/dts/variant/m0054/var02.1/m0xxx-modalai-camera.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0xxx-modalai-camera.dtsi
         cp ${WORKDIR}/dts/variant/m0054/var02.1/m0054-modalai-camera.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0054-modalai-camera.dtsi
         cp ${WORKDIR}/dts/variant/m0054/var02.1/m0054-modalai-platform.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0054-modalai-platform.dtsi
      fi
   elif  [[ "${MACHINE}" == "m0104" ]]; then
      if  [[ "${VARIANT}" == "var02" ]]; then
         bbnote "For machine: ${MACHINE} applying variant: ${VARIANT}"
         cp ${WORKDIR}/dts/variant/m0104/var02/m0104-modalai-platform.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0104-modalai-platform.dtsi
         cp ${WORKDIR}/dts/variant/m0104/var02/m0xxx-modalai-camera.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0xxx-modalai-camera.dtsi
      elif  [[ "${VARIANT}" == "var00.2" ]]; then
         bbnote "For machine: ${MACHINE} applying variant: ${VARIANT}"
         cp ${WORKDIR}/dts/variant/m0104/var00.2/m0xxx-kona-pinctrl.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0xxx-kona-pinctrl.dtsi
         cp ${WORKDIR}/dts/variant/m0104/var00.2/m0xxx-modalai-gpio.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0xxx-modalai-gpio.dtsi
         cp ${WORKDIR}/dts/variant/m0104/var00.2/m0104-modalai-camera.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0104-modalai-camera.dtsi
         cp ${WORKDIR}/dts/variant/m0104/var00.2/m0104-modalai-platform.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0104-modalai-platform.dtsi
      fi
   fi

}

do_configure_prepend() {
   # We merge the config changes with the default config for the board
   # using merge-config.sh kernel tool

   mergeTool=${S}/scripts/kconfig/merge_config.sh
   confDir=${S}/arch/${ARCH}/configs
   defconf=${confDir}/${KERNEL_CONFIG}

   ${mergeTool} -m -O ${confDir} ${defconf} ${CFG_FRAGMENTS}

   # The output will be named .config. We rename it back to ${defconf} because
   # that's what the rest of do_configure expects
   mv ${confDir}/.config ${defconf}
   bbnote "Writing back the merged config: ${confDir}/.config to ${defconf}"
}

EXTRA_OEMAKE += "PLATFORM_TYPE=${MACHINE}"