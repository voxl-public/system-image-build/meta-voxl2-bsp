#include <linux/cdev.h>
#include <linux/delay.h>
#include <linux/fs.h>
#include <linux/gpio.h>
#include <linux/hrtimer.h>
#include <linux/kernel.h>
#include <linux/ktime.h>
#include <linux/module.h>
#include <linux/uaccess.h>
#include <linux/sysfs.h>
#include <linux/platform_device.h>
#include <linux/of.h>
#include <linux/version.h>

#define DRIVER_VERSION "0.0.4"
#define DEVICE_NAME "voxl-fsync"

/* 
 * GPIO 109 is used with M0173 and is first to utilize this feature, default to GPIO 109
 * 
 * After the module is loaded:
 * /sys/module/voxl_fsync_mod/parameters/sampling_period_ns
 *  - ulong - the period (1/FPS) in nanoseconds, default 33333333, 33.3ms, 30 FPS
 * 
 * /sys/module/voxl_fsync_mod/parameters/pulse_width_us
 *  - uint - the length of pulse to use in microseconds, default 10, 10us
 * 
 * /sys/module/voxl_fsync_mod/parameters/gpio_num
 *  - uint - the GPIO number to use
 */
#define GPIO_OFFSET 1100

/* Sample Period (ns) (1/FPS) */
static ulong sampling_period_ns = 33333333; // 33.3ms
module_param(sampling_period_ns, ulong, 0644);

/* Sync pulse width (us) */
static uint pulse_width_us = 10; //
module_param(pulse_width_us, uint, 0644);

/* GPIO number */
static uint gpio_num = 109;
module_param(gpio_num, uint, 0644);

/* enable/disable the GPIO output and timestamps */
static int enabled = 0;
module_param(enabled, int, 0644);
static int initialized = 0;

static int fsync_dev_major;
static struct class *fsync_class;

static int fsync_open_count = 0;
static DEFINE_MUTEX(fysnc_mutex);
static int fsync_update_pending;
static DECLARE_WAIT_QUEUE_HEAD(fsync_wait_queue);

static struct hrtimer fsync_hrtimer;
static int64_t fsync_time_mon_ns;


/*
 *
 */
static int voxl_fsync_probe(struct platform_device *pdev);
static int voxl_fsync_remove(struct platform_device *pdev);

static const struct of_device_id of_voxl_fsync_dt_match[] = {
	{.compatible	= "modalai,voxl-fsync"},
	{},
};

static struct platform_driver voxl_fsync_driver = {
	.probe	= voxl_fsync_probe,
	.remove	= voxl_fsync_remove,
	.driver	= {
		.name	= "voxl-fsync",
		.of_match_table	= of_voxl_fsync_dt_match,
	},
};

MODULE_DEVICE_TABLE(of, of_voxl_fsync_dt_match);

/*
 *
 */
static int fsync_open(struct inode *inode, struct file *file);
static int fsync_release(struct inode *inode, struct file *file);
static ssize_t fsync_read(struct file *file, char __user *buf, size_t count, loff_t *offset);

static const struct file_operations fsync_fops = {
	.owner = THIS_MODULE,
	.open = fsync_open,
	.release = fsync_release,
	.read = fsync_read
};

static int fsync_open(struct inode *inode, struct file *file)
{
	if (fsync_open_count > 0) {
		return -EBUSY;
	}
	fsync_open_count++;
	return 0;
}

static int fsync_release(struct inode *inode, struct file *file)
{
	fsync_open_count--;
	return 0;
}

static ssize_t fsync_read(struct file *file, char __user *buf, size_t count, loff_t *offset)
{
	char tmp_buf[32];
	int64_t tmp_ns;
	size_t tmp_len;

	/* wait here until we get timestamp updates in the timer tick handler */
	wait_event_interruptible(fsync_wait_queue, fsync_update_pending != 0);
	fsync_update_pending = 0;

	/* maybe unnessary as this is int64 and threadsafe already ?*/
	mutex_lock(&fysnc_mutex);
	tmp_ns = fsync_time_mon_ns;
	mutex_unlock(&fysnc_mutex);

	snprintf(tmp_buf, sizeof(tmp_buf), "%lld\n", tmp_ns);
	
	tmp_len = strlen(tmp_buf);
	if (count > tmp_len) {
		count = tmp_len;
	}
	
	if (copy_to_user(buf, tmp_buf, count)) {
		return -EFAULT;
	}

	return count;
}


static enum hrtimer_restart fsync_hrtimer_tick_cb(struct hrtimer *timer)
{
	/* initialize on being enabled */
	if (!initialized && enabled) {
		gpio_direction_output((gpio_num + GPIO_OFFSET), 0);
		gpio_export((gpio_num + GPIO_OFFSET), true);
		initialized = true;
		pr_info("voxl-fsync: initialized\n");
	}

	/* re-start timer now to keep periodic */
	hrtimer_forward_now(&fsync_hrtimer, ns_to_ktime(sampling_period_ns));
	
	if (enabled) {
		gpio_set_value((gpio_num + GPIO_OFFSET), 1);

		/* TIMESTAMP TIMESTAMP TIMESTAMP*/
		mutex_lock(&fysnc_mutex);
		fsync_time_mon_ns = ktime_to_ns(ktime_get());
		mutex_unlock(&fysnc_mutex);
		
		if(pulse_width_us > 0)
			udelay(pulse_width_us);

		gpio_set_value((gpio_num + GPIO_OFFSET), 0);

		/* signal clients */
		fsync_update_pending = 1;
		wake_up_interruptible(&fsync_wait_queue);
	} else {
		initialized = false;
	}
	
	return HRTIMER_RESTART;
}

static int voxl_fsync_probe(struct platform_device *pdev)
{
	struct device *dev = NULL;

	pr_info("voxl_fsync_probe\n");

	fsync_dev_major = register_chrdev(0, DEVICE_NAME, &fsync_fops);
	if (fsync_dev_major < 0) {
		pr_err(DEVICE_NAME ": unable to get major %d\n", fsync_dev_major);
		return fsync_dev_major;
	}

	fsync_class = class_create(THIS_MODULE, DEVICE_NAME);
	if (IS_ERR(fsync_class)) {
		pr_err(DEVICE_NAME ": unable to create class\n");
		return PTR_ERR(fsync_class);
	}

	dev = device_create(fsync_class, NULL, MKDEV(fsync_dev_major, 0), NULL, DEVICE_NAME);
	if (IS_ERR(dev)) {
		pr_err(DEVICE_NAME ": unable to create device\n");
		return PTR_ERR(dev);
	}

	hrtimer_init(&fsync_hrtimer, CLOCK_MONOTONIC, HRTIMER_MODE_REL);
	fsync_hrtimer.function = &fsync_hrtimer_tick_cb;
	hrtimer_start(&fsync_hrtimer, ns_to_ktime(sampling_period_ns), HRTIMER_MODE_REL);

	pr_info("voxl_fsync_probe : OK\n");
	return 0;
}

static int voxl_fsync_remove(struct platform_device *pdev)
{
	pr_info("voxl_fsync_remove\n");
	platform_driver_unregister(&voxl_fsync_driver);
	return 0;
}

static int __init voxl_fsync_mod_init(void)
{
	pr_info("voxl_fsync_mod_init\n");
	return platform_driver_register(&voxl_fsync_driver);
}

static 	void __exit voxl_fsync_mod_exit(void)
{
	device_destroy(fsync_class, MKDEV(fsync_dev_major, 0));
	class_destroy(fsync_class);
	unregister_chrdev(fsync_dev_major, DEVICE_NAME);
	hrtimer_cancel(&fsync_hrtimer);
}

module_init(voxl_fsync_mod_init);
module_exit(voxl_fsync_mod_exit);


MODULE_DESCRIPTION("GPIO pulse and timestamp generation module");
MODULE_AUTHOR("Travis Bottalico <travis@modalai.com>");
MODULE_VERSION(DRIVER_VERSION);
MODULE_LICENSE("GPL");