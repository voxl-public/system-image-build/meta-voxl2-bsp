#include "m0xxx-modalai-camera.dtsi"

&tlmm {
	m0054_cam_sensor_0_mclk_active: m0054_cam_sensor_0_mclk_active {
		/* MCLK0 */
		mux {
			pins = "gpio94";
			function = "cam_mclk";
		};
		config {
			pins = "gpio94";
			bias-disable; /* No PULL */
			drive-strength = <8>; /* 8 MA */
		};
	};
	m0054_cam_sensor_0_mclk_suspend: m0054_cam_sensor_0_mclk_suspend {
		/* MCLK0 */
		mux {
			pins = "gpio94";
			function = "cam_mclk";
		};
		config {
			pins = "gpio94";
			bias-pull-down; /* PULL DOWN */
			drive-strength = <8>; /* 8 MA */
		};
	};
	m0054_cam_sensor_0_reset_active: m0054_cam_sensor_0_reset_active {
		mux {
			pins = "gpio93";
			function = "gpio";
		};
		config {
			pins = "gpio93";
			bias-disable; /* No PULL */
			drive-strength = <8>; /* 8 MA */
		};
	};
	m0054_cam_sensor_0_reset_suspend: m0054_cam_sensor_0_reset_suspend {
		mux {
			pins = "gpio93";
			function = "gpio";
		};
		config {
			pins = "gpio93";
			bias-pull-down; /* PULL DOWN */
			drive-strength = <8>; /* 8 MA */
			output-low;
		};
	};

	m0054_cam_sensor_1_mclk_active: m0054_cam_sensor_1_mclk_active {
		/* MCLK1 */
		mux {
			pins = "gpio95";
			function = "cam_mclk";
		};
		config {
			pins = "gpio95";
			bias-disable; /* No PULL */
			drive-strength = <8>; /* 8 MA */
		};
	};
	m0054_cam_sensor_1_mclk_suspend: m0054_cam_sensor_1_mclk_suspend {
		/* MCLK1 */
		mux {
			pins = "gpio95";
			function = "cam_mclk";
		};
		config {
			pins = "gpio95";
			bias-pull-down; /* PULL DOWN */
			drive-strength = <8>; /* 8 MA */
		};
	};
	/* Shared with cam_sensor_5, see qcom,cam-res-mgr */
	m0054_cam_sensor_1_5_reset_active: m0054_cam_sensor_1_5_reset_active {
		mux {
			pins = "gpio100";
			function = "gpio";
		};
		config {
			pins = "gpio100";
			bias-disable; /* No PULL */
			drive-strength = <8>; /* 8 MA */
		};
	};
	/* Shared with cam_sensor_5, see qcom,cam-res-mgr */
	m0054_cam_sensor_1_5_reset_suspend: m0054_cam_sensor_1_5_reset_suspend {
		mux {
			pins = "gpio100";
			function = "gpio";
		};
		config {
			pins = "gpio100";
			bias-pull-down; /* PULL DOWN */
			drive-strength = <8>; /* 8 MA */
			output-low;
		};
	};

	m0054_cam_sensor_2_mclk_active: m0054_cam_sensor_2_mclk_active {
		/* MCLK2 */
		mux {
			pins = "gpio96";
			function = "cam_mclk";
		};
		config {
			pins = "gpio96";
			bias-disable; /* No PULL */
			drive-strength = <2>; /* 2 MA */
		};
	};
	m0054_cam_sensor_2_mclk_suspend: m0054_cam_sensor_2_mclk_suspend {
		/* MCLK2 */
		mux {
			pins = "gpio96";
			function = "cam_mclk";
		};
		config {
			pins = "gpio96";
			bias-pull-down; /* PULL DOWN */
			drive-strength = <2>; /* 2 MA */
		};
	};
	m0054_cam_sensor_2_reset_active: m0054_cam_sensor_2_reset_active {
		mux {
			pins = "gpio92";
			function = "gpio";
		};
		config {
			pins = "gpio92";
			bias-disable; /* No PULL */
			drive-strength = <8>; /* 8 MA */
		};
	};
	m0054_cam_sensor_2_reset_suspend: m0054_cam_sensor_2_reset_suspend {
		mux {
			pins = "gpio92";
			function = "gpio";
		};
		config {
			pins = "gpio92";
			bias-pull-down; /* PULL DOWN */
			drive-strength = <8>; /* 8 MA */
			output-low;
		};
	};

	m0054_cam_sensor_3_mclk_active: m0054_cam_sensor_3_mclk_active {
		/* MCLK3 */
		mux {
			pins = "gpio97";
			function = "cam_mclk";
		};
		config {
			pins = "gpio97";
			bias-disable; /* No PULL */
			drive-strength = <2>; /* 2 MA */
		};
	};
	m0054_cam_sensor_3_mclk_suspend: m0054_cam_sensor_3_mclk_suspend {
		/* MCLK3 */
		mux {
			pins = "gpio97";
			function = "cam_mclk";
		};
		config {
			pins = "gpio97";
			bias-pull-down; /* PULL DOWN */
			drive-strength = <2>; /* 2 MA */
		};
	};
	m0054_cam_sensor_3_reset_active: m0054_cam_sensor_3_reset_active {
		mux {
			pins = "gpio109";
			function = "gpio";
		};

		config {
			pins = "gpio109";
			bias-disable; /* No PULL */
			drive-strength = <2>; /* 2 MA */
		};
	};
	m0054_cam_sensor_3_reset_suspend: m0054_cam_sensor_3_reset_suspend {
		mux {
			pins = "gpio109";
			function = "gpio";
		};
		config {
			pins = "gpio109";
			bias-pull-down; /* PULL DOWN */
			drive-strength = <2>; /* 2 MA */
			output-low;
		};
	};

	m0054_cam_sensor_4_mclk_active: m0054_cam_sensor_4_mclk_active {
		/* MCLK4 */
		mux {
			pins = "gpio98";
			function = "cam_mclk";
		};

		config {
			pins = "gpio98";
			bias-disable; /* No PULL */
			drive-strength = <2>; /* 2 MA */
		};
	};
	m0054_cam_sensor_4_mclk_suspend: m0054_cam_sensor_4_mclk_suspend {
		/* MCLK4 */
		mux {
			pins = "gpio98";
			function = "cam_mclk";
		};

		config {
			pins = "gpio98";
			bias-pull-down; /* PULL DOWN */
			drive-strength = <2>; /* 2 MA */
		};
	};
	m0054_cam_sensor_4_reset_active: m0054_cam_sensor_4_reset_active {
		mux {
			pins = "gpio78";
			function = "gpio";
		};

		config {
			pins = "gpio78";
			bias-disable; /* No PULL */
			drive-strength = <2>; /* 2 MA */
		};
	};
	m0054_cam_sensor_4_reset_suspend: m0054_cam_sensor_4_reset_suspend {
		mux {
			pins = "gpio78";
			function = "gpio";
		};

		config {
			pins = "gpio78";
			bias-pull-down; /* PULL DOWN */
			drive-strength = <2>; /* 2 MA */
			output-low;
		};
	};

	m0054_cam_sensor_5_mclk_active: m0054_cam_sensor_5_mclk_active {
		/* MCLK5 */
		mux {
			pins = "gpio99";
			function = "cam_mclk";
		};
		config {
			pins = "gpio99";
			bias-disable; /* No PULL */
			drive-strength = <2>; /* 2 MA */
		};
	};
	m0054_cam_sensor_5_mclk_suspend: m0054_cam_sensor_5_mclk_suspend {
		/* MCLK5 */
		mux {
			pins = "gpio99";
			function = "cam_mclk";
		};
		config {
			pins = "gpio99";
			bias-pull-down; /* PULL DOWN */
			drive-strength = <2>; /* 2 MA */
		};
	};
	/* see m0054_cam_sensor_1_5_reset_active */
	/* see m0054_cam_sensor_1_5_reset_suspend */

};

&soc {
	/* Shared with cam_sensor_1 and cam_sensor_5 */
	qcom,cam-res-mgr {
		compatible = "qcom,cam-res-mgr";
		status = "ok";
		shared-gpios = <1200>;
		pinctrl-names = "cam_res_mgr_default", "cam_res_mgr_suspend";
		pinctrl-0 = <&m0054_cam_sensor_1_5_reset_active>;
		pinctrl-1 = <&m0054_cam_sensor_1_5_reset_suspend>;
	};
};

&cam_cci0 {

	/*
	 * CCI0 and CSI0 (combo mode)
	 */
	qcom,cam-sensor0 {
		cell-index = <0>;
		compatible = "qcom,cam-sensor";
		csiphy-sd-index = <0>;
		sensor-position-roll = <0>;
		sensor-position-pitch = <0>;
		sensor-position-yaw = <0>;
		cam_vio-supply = <&pm8009_l7>;
		cam_vana-supply = <&pm8009_l5>;
		cam_vdig-supply = <&pm8009_l1>;
		cam_clk-supply = <&titan_top_gdsc>;
		regulator-names = "cam_vio", "cam_vana", "cam_vdig",
			"cam_clk";
		rgltr-cntrl-support;
		pwm-switch;
		rgltr-min-voltage = <1800000 2800000 1104000 0>;
		rgltr-max-voltage = <1800000 3000000 1104000 0>;
		rgltr-load-current = <120000 80000 1200000 0>;
		gpio-no-mux = <0>;
		pinctrl-names = "cam_default", "cam_suspend";
		pinctrl-0 = <&m0054_cam_sensor_0_mclk_active
				     &m0054_cam_sensor_0_reset_active>;
		pinctrl-1 = <&m0054_cam_sensor_0_mclk_suspend
				     &m0054_cam_sensor_0_reset_suspend>;
		gpios = <&tlmm 94 0>,
			    <&tlmm 93 0>;
		gpio-reset = <1>;
		gpio-req-tbl-num = <0 1>;
		gpio-req-tbl-flags = <1 0>;
		gpio-req-tbl-label = "CAMIF_MCLK0",
					"CAM_RESET0";
		sensor-mode = <0>;
		cci-master = <0>;
		status = "ok";
		clocks = <&clock_camcc CAM_CC_MCLK0_CLK>;
		clock-names = "cam_clk";
		clock-cntl-level = "turbo";
		clock-rates = <24000000>;
	};

	/*
	 * CCI1 and CSI0 (combo mode)
	 */
	qcom,cam-sensor1 {
		cell-index = <1>;
		compatible = "qcom,cam-sensor";
		csiphy-sd-index = <0>;
		sensor-position-roll = <0>;
		sensor-position-pitch = <0>;
		sensor-position-yaw = <0>;
		cam_vio-supply = <&pm8009_l7>;
		cam_vana-supply = <&pm8009_l5>;
		cam_vdig-supply = <&pm8009_l1>;
		cam_clk-supply = <&titan_top_gdsc>;
		regulator-names = "cam_vio", "cam_vana", "cam_vdig",
			"cam_clk";
		rgltr-cntrl-support;
		pwm-switch;
		rgltr-min-voltage = <1800000 2800000 1104000 0>;
		rgltr-max-voltage = <1800000 3000000 1104000 0>;
		rgltr-load-current = <120000 80000 1200000 0>;
		gpio-no-mux = <0>;
		/* See qcom,cam-res-mgr for shared GPIO */
		pinctrl-names = "cam_default", "cam_suspend";
		pinctrl-0 = <&m0054_cam_sensor_1_mclk_active>;
		/*		     &m0054_cam_sensor_1_5_reset_suspend>;  */
		pinctrl-1 = <&m0054_cam_sensor_1_mclk_suspend>;
		/*		     &m0054_cam_sensor_1_5_reset_suspend>; */
		gpios = <&tlmm 95 0>,
			    <&tlmm 100 0>;
		gpio-reset = <1>;
		gpio-req-tbl-num = <0 1>;
		gpio-req-tbl-flags = <1 0>;
		gpio-req-tbl-label = "CAMIF_MCLK0",
					"CAM_RESET0";
		sensor-mode = <0>;
		cci-master = <1>;
		status = "ok";
		clocks = <&clock_camcc CAM_CC_MCLK1_CLK>;
		clock-names = "cam_clk";
		clock-cntl-level = "turbo";
		clock-rates = <24000000>;
	};

	/*
	 * CCI1 and CSI4 (combo mode)
	 */
	qcom,cam-sensor4 {
		cell-index = <4>;
		compatible = "qcom,cam-sensor";
		csiphy-sd-index = <4>;
		sensor-position-roll = <0>;
		sensor-position-pitch = <0>;
		sensor-position-yaw = <0>;
		cam_vio-supply = <&pm8009_l7>;
		cam_vana-supply = <&pm8009_l6>;
		cam_vdig-supply = <&pm8009_l1>;
		cam_clk-supply = <&titan_top_gdsc>;
		regulator-names = "cam_vio", "cam_vana", "cam_vdig",
					"cam_clk";
		rgltr-cntrl-support;
		pwm-switch;
		rgltr-min-voltage = <1800000 2800000 1104000 0>;
		rgltr-max-voltage = <1800000 3000000 1104000 0>;
		rgltr-load-current = <120000 80000 1200000 0>;
		gpio-no-mux = <0>;
		pinctrl-names = "cam_default", "cam_suspend";
		pinctrl-0 = <&m0054_cam_sensor_4_mclk_active
				     &m0054_cam_sensor_4_reset_active>;
		pinctrl-1 = <&m0054_cam_sensor_4_mclk_suspend
				     &m0054_cam_sensor_4_reset_suspend>;
		gpios = <&tlmm 98 0>,
			<&tlmm 78 0>;
		gpio-reset = <1>;
		gpio-req-tbl-num = <0 1>;
		gpio-req-tbl-flags = <1 0>;
		gpio-req-tbl-label = "CAMIF_MCLK4",
					"CAM_RESET4";
		sensor-mode = <0>;
		cci-master = <1>;
		status = "ok";
		clocks = <&clock_camcc CAM_CC_MCLK4_CLK>;
		clock-names = "cam_clk";
		clock-cntl-level = "turbo";
		clock-rates = <24000000>;
	};
};

&cam_cci1 {

	/*
	 * CCI2 and CSI2
	 */
	qcom,cam-sensor2 {
		cell-index = <2>;
		compatible = "qcom,cam-sensor";
		csiphy-sd-index = <2>;
		sensor-position-roll = <0>;
		sensor-position-pitch = <0>;
		sensor-position-yaw = <0>;
		cam_vio-supply = <&pm8009_l7>;
		cam_vana-supply = <&pm8009_l6>;
		cam_vdig-supply = <&pm8009_l1>;
		cam_clk-supply = <&titan_top_gdsc>;
		regulator-names = "cam_vio", "cam_vana", "cam_vdig",
			"cam_clk";
		rgltr-cntrl-support;
		pwm-switch;
		rgltr-min-voltage = <1800000 2800000 1104000 0>;
		rgltr-max-voltage = <1800000 3000000 1104000 0>;
		rgltr-load-current = <120000 80000 1200000 0>;
		gpio-no-mux = <0>;
		pinctrl-names = "cam_default", "cam_suspend";
		pinctrl-0 = <&m0054_cam_sensor_2_mclk_active
				     &m0054_cam_sensor_2_reset_active>;
		pinctrl-1 = <&m0054_cam_sensor_2_mclk_suspend
				     &m0054_cam_sensor_2_reset_suspend>;
		gpios = <&tlmm 96 0>,
			<&tlmm 92 0>;
		gpio-reset = <1>;
		gpio-req-tbl-num = <0 1>;
		gpio-req-tbl-flags = <1 0>;
		gpio-req-tbl-label = "CAMIF_MCLK2",
					"CAM_RESET2";
		sensor-mode = <0>;
		cci-master = <0>;
		status = "ok";
		clocks = <&clock_camcc CAM_CC_MCLK2_CLK>;
		clock-names = "cam_clk";
		clock-cntl-level = "turbo";
		clock-rates = <24000000>;
	};

	/*
	 * CCI3 and CSI3
	 */
	qcom,cam-sensor3 {
		cell-index = <3>;
		compatible = "qcom,cam-sensor";
		csiphy-sd-index = <3>;
		sensor-position-roll = <0>;
		sensor-position-pitch = <0>;
		sensor-position-yaw = <0>;
		cam_vio-supply = <&pm8009_l7>;
		cam_vana-supply = <&pm8009_l6>;
		cam_vdig-supply = <&pm8009_l1>;
		cam_clk-supply = <&titan_top_gdsc>;
		regulator-names = "cam_vio", "cam_vana", "cam_vdig",
			"cam_clk";
		rgltr-cntrl-support;
		pwm-switch;
		rgltr-min-voltage = <1800000 2800000 1104000 0>;
		rgltr-max-voltage = <1800000 3000000 1104000 0>;
		rgltr-load-current = <120000 80000 1200000 0>;
		gpio-no-mux = <0>;
		pinctrl-names = "cam_default", "cam_suspend";
		pinctrl-0 = <&m0054_cam_sensor_3_mclk_active
				     &m0054_cam_sensor_3_reset_active>;
		pinctrl-1 = <&m0054_cam_sensor_3_mclk_suspend
				     &m0054_cam_sensor_3_reset_suspend>;
		gpios = <&tlmm 97 0>,
			<&tlmm 109 0>;
		gpio-reset = <1>;
		gpio-req-tbl-num = <0 1>;
		gpio-req-tbl-flags = <1 0>;
		gpio-req-tbl-label = "CAMIF_MCLK3",
					"CAM_RESET3";
		sensor-mode = <0>;
		cci-master = <1>;
		status = "ok";
		clocks = <&clock_camcc CAM_CC_MCLK3_CLK>;
		clock-names = "cam_clk";
		clock-cntl-level = "turbo";
		clock-rates = <24000000>;
	};

	/*
	 * CCI3 and CSI4 (combo mode)
	 */
	qcom,cam-sensor5 {
		cell-index = <5>;
		compatible = "qcom,cam-sensor";
		csiphy-sd-index = <4>;
		sensor-position-roll = <0>;
		sensor-position-pitch = <0>;
		sensor-position-yaw = <0>;
		cam_vio-supply = <&pm8009_l7>;
		cam_vana-supply = <&pm8009_l6>;
		cam_vdig-supply = <&pm8009_l1>;
		cam_clk-supply = <&titan_top_gdsc>;
		regulator-names = "cam_vio", "cam_vana", "cam_vdig",
			"cam_clk";
		rgltr-cntrl-support;
		pwm-switch;
		rgltr-min-voltage = <1800000 2800000 1104000 0>;
		rgltr-max-voltage = <1800000 3000000 1104000 0>;
		rgltr-load-current = <120000 80000 1200000 0>;
		gpio-no-mux = <0>;
		/* See qcom,cam-res-mgr for shared GPIO */
		pinctrl-names = "cam_default", "cam_suspend";
		pinctrl-0 = <&m0054_cam_sensor_5_mclk_active>;
		/* 		     &m0054_cam_sensor_1_5_reset_suspend>;  */
		pinctrl-1 = <&m0054_cam_sensor_5_mclk_suspend>;
		/* 		     &m0054_cam_sensor_1_5_reset_suspend>; */
		gpios = <&tlmm 99 0>,
			    <&tlmm 100 0>;
		gpio-reset = <1>;
		gpio-req-tbl-num = <0 1>;
		gpio-req-tbl-flags = <1 0>;
		gpio-req-tbl-label = "CAMIF_MCLK5",
					"CAM_RESET6";
		sensor-mode = <0>;
		cci-master = <1>;
		status = "ok";
		clocks = <&clock_camcc CAM_CC_MCLK5_CLK>;
		clock-names = "cam_clk";
		clock-cntl-level = "turbo";
		clock-rates = <24000000>;
	};

};

/*
 * Include at end of file to override
 *
 */
#include "m0054-modalai-camera-overlay.dtsi"
