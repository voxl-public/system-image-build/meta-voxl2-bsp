#include <linux/init.h>
#include <linux/module.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/uaccess.h>

#include <linux/platform_device.h>
#include <linux/of.h>
#include <linux/of_gpio.h>
#include <linux/gpio.h>
#include <linux/kernel.h>

#define DRIVER_VERSION "0.1.1"
#define DEVICE_NAME "voxl-gpio"

#define GPIO_OFFSET 1100

static int voxl_gpio_dev_major;
static struct class *voxl_gpio_class;

static const struct file_operations voxl_gpio_fops = {
	.owner		= THIS_MODULE,
};

static int voxl_gpio_probe(struct platform_device *pdev)
{
	struct device *dev = NULL;
	struct device_node *of_node = NULL;
	uint32_t gpio_num;
	int count, i, ret;
	const char *gpio_name;

	pr_info("voxl_gpio_probe\n");

	voxl_gpio_dev_major = register_chrdev(0, DEVICE_NAME, &voxl_gpio_fops);
	if (voxl_gpio_dev_major < 0) {
		pr_err(DEVICE_NAME ": unable to get major %d\n", voxl_gpio_dev_major);
		return voxl_gpio_dev_major;
	}

	voxl_gpio_class = class_create(THIS_MODULE, DEVICE_NAME);
	if (IS_ERR(voxl_gpio_class)) {
		pr_err(DEVICE_NAME ": unable to create class\n");
		return PTR_ERR(voxl_gpio_class);
	}

	dev = device_create(voxl_gpio_class, NULL, MKDEV(voxl_gpio_dev_major, 0), NULL, DEVICE_NAME);
	if (IS_ERR(dev)) {
		pr_err(DEVICE_NAME ": unable to create device\n");
		return PTR_ERR(dev);
	}

	if(!pdev->dev.of_node) {
		pr_err(DEVICE_NAME ": Device node not found\n");
		return -ENODEV;
	}

	of_node = pdev->dev.of_node;

	count = of_property_count_u32_elems(of_node, "modalai,gpio-init-output-high");
	for (i = 0; i < count; i++) {

		of_property_read_u32_index(of_node, "modalai,gpio-init-output-high", i, &gpio_num);
		gpio_name = kasprintf(GFP_KERNEL, "gpio%d", gpio_num);
		ret = gpio_request((gpio_num + GPIO_OFFSET), gpio_name);
		if (ret) {
			pr_err(DEVICE_NAME ": Failed to request GPIO %u with name %s\n", gpio_num, gpio_name);
			kfree(gpio_name);
			continue;
		}

		ret = gpio_direction_output((gpio_num + GPIO_OFFSET), 1);
		if (ret) {
			pr_err(DEVICE_NAME ": Failed to set GPIO %u as output high\n", gpio_num);
			gpio_free((gpio_num + GPIO_OFFSET));
			kfree(gpio_name);
			continue;
		}

		ret = gpio_export((gpio_num + GPIO_OFFSET), true);
		if (ret) {
			pr_err(DEVICE_NAME ": Failed to export GPIO %u for output high\n", gpio_num);
			gpio_free((gpio_num + GPIO_OFFSET));
			kfree(gpio_name);
			continue;
		}

		pr_info(DEVICE_NAME ": %s - output, high\n", gpio_name);

		kfree(gpio_name);
	}
	
	count = of_property_count_u32_elems(of_node, "modalai,gpio-init-output-low");
	for (i = 0; i < count; i++) {

		of_property_read_u32_index(of_node, "modalai,gpio-init-output-low", i, &gpio_num);
		gpio_name = kasprintf(GFP_KERNEL, "gpio%d", gpio_num);
		ret = gpio_request((gpio_num + GPIO_OFFSET), gpio_name);
		if (ret) {
			pr_err(DEVICE_NAME ": Failed to request GPIO %u with name %s\n", gpio_num, gpio_name);
			kfree(gpio_name);
			continue;
		}

		ret = gpio_direction_output((gpio_num + GPIO_OFFSET), 0);
		if (ret) {
			pr_err(DEVICE_NAME ": Failed to set GPIO %u as output low\n", gpio_num);
			gpio_free((gpio_num + GPIO_OFFSET));
			kfree(gpio_name);
			continue;
		}

		ret = gpio_export((gpio_num + GPIO_OFFSET), true);
		if (ret) {
			pr_err(DEVICE_NAME ": Failed to export GPIO %u for output low\n", gpio_num);
			gpio_free((gpio_num + GPIO_OFFSET));
			kfree(gpio_name);
			continue;
		}

		pr_info(DEVICE_NAME ": %s - output, low\n", gpio_name);
	}

	count = of_property_count_u32_elems(of_node, "modalai,gpio-init-input");
	for (i = 0; i < count; i++) {

		of_property_read_u32_index(of_node, "modalai,gpio-init-input", i, &gpio_num);
		gpio_name = kasprintf(GFP_KERNEL, "gpio%d", gpio_num);
		ret = gpio_request((gpio_num + GPIO_OFFSET), gpio_name);
		if (ret) {
			pr_err(DEVICE_NAME ": Failed to request GPIO %u with name %s\n", gpio_num, gpio_name);
			kfree(gpio_name);
			continue;
		}

		ret = gpio_direction_input((gpio_num + GPIO_OFFSET));
		if (ret) {
			pr_err(DEVICE_NAME ": Failed to set GPIO %u as input\n", gpio_num);
			gpio_free((gpio_num + GPIO_OFFSET));
			kfree(gpio_name);
			continue;
		}

		ret = gpio_export((gpio_num + GPIO_OFFSET), true);
		if (ret) {
			pr_err(DEVICE_NAME ": Failed to export GPIO %u for input\n", gpio_num);
			gpio_free((gpio_num + GPIO_OFFSET));
			kfree(gpio_name);
			continue;
		}

		pr_info(DEVICE_NAME ": %s - input\n", gpio_name);
	}

	pr_warn(DEVICE_NAME " probe complete");

	return 0;
}

static int voxl_gpio_remove(struct platform_device *pdev)
{
	pr_warn(DEVICE_NAME " remove\n");
	device_destroy(voxl_gpio_class, MKDEV(voxl_gpio_dev_major, 0));
	class_destroy(voxl_gpio_class);
	unregister_chrdev(voxl_gpio_dev_major, DEVICE_NAME);
	return 0;
}

static const struct of_device_id of_voxl_gpio_dt_match[] = {
	{.compatible	= "modalai,voxl-gpio"},
	{},
};

MODULE_DEVICE_TABLE(of, of_voxl_gpio_dt_match);

static struct platform_driver voxl_gpio_driver = {
	.probe	= voxl_gpio_probe,
	.remove	= voxl_gpio_remove,
	.driver	= {
		.name	= "voxl_gpio",
		.of_match_table	= of_voxl_gpio_dt_match,
	},
};

static int __init voxl_gpio_mod_init(void)
{
	pr_warn(DEVICE_NAME " init\n");
	return platform_driver_register(&voxl_gpio_driver);
}

static void __exit voxl_gpio_mod_exit(void)
{
	pr_warn(DEVICE_NAME " exit\n");
	platform_driver_unregister(&voxl_gpio_driver);
}

module_init(voxl_gpio_mod_init);
module_exit(voxl_gpio_mod_exit);

MODULE_DESCRIPTION("GPIO initialization module");
MODULE_AUTHOR("Travis Bottalico <travis@modalai.com>");
MODULE_VERSION(DRIVER_VERSION);
MODULE_LICENSE("GPL");
