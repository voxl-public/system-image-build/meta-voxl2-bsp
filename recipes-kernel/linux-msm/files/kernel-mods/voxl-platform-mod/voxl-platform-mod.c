#include <linux/init.h>
#include <linux/module.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/uaccess.h>

#include <linux/platform_device.h>
#include <linux/of.h>
#include <linux/of_gpio.h>
#include <linux/gpio.h>
#include <linux/kernel.h>

#define DRIVER_VERSION "0.0.2"
#define DEVICE_NAME "voxl-platform"

/* machine */
#define IDX_MACHINE 0
static uint machine = 0;
module_param(machine, uint, 0644);
/* variant */
#define IDX_VARIANT 1
static uint variant = 0;
module_param(variant, uint, 0644);
/* config */
#define IDX_CONFIG 2
static uint config = 0;
module_param(config, uint, 0644);

static int voxl_platform_dev_major;
static struct class *voxl_platform_class;

static const struct file_operations voxl_platform_fops = {
	.owner		= THIS_MODULE,
};

static int voxl_platform_probe(struct platform_device *pdev)
{
	struct device *dev = NULL;
	struct device_node *of_node = NULL;
	uint32_t temp;
	int count, i;
	const char *platform_name;

	pr_info("voxl_platform_probe\n");

	voxl_platform_dev_major = register_chrdev(0, DEVICE_NAME, &voxl_platform_fops);
	if (voxl_platform_dev_major < 0) {
		pr_err(DEVICE_NAME ": unable to get major %d\n", voxl_platform_dev_major);
		return voxl_platform_dev_major;
	}

	voxl_platform_class = class_create(THIS_MODULE, DEVICE_NAME);
	if (IS_ERR(voxl_platform_class)) {
		pr_err(DEVICE_NAME ": unable to create class\n");
		return PTR_ERR(voxl_platform_class);
	}

	dev = device_create(voxl_platform_class, NULL, MKDEV(voxl_platform_dev_major, 0), NULL, DEVICE_NAME);
	if (IS_ERR(dev)) {
		pr_err(DEVICE_NAME ": unable to create device\n");
		return PTR_ERR(dev);
	}

	if(!pdev->dev.of_node) {
		pr_err(DEVICE_NAME ": Device node not found\n");
		return -ENODEV;
	}

	of_node = pdev->dev.of_node;

	count = of_property_count_u32_elems(of_node, "modalai,platform-id");
	for (i = 0; i < count; i++) {

		of_property_read_u32_index(of_node, "modalai,platform-id", i, &temp);
		platform_name = kasprintf(GFP_KERNEL, "- ID - %d", temp);
		
		if(i == IDX_MACHINE){
			machine = temp;
			pr_info(DEVICE_NAME ": Machine: %s\n", platform_name);
		}
		else if(i == IDX_VARIANT){
			variant = temp;
			pr_info(DEVICE_NAME ": Variant: %s\n", platform_name);
		}
		else if(i == IDX_CONFIG){
			config = temp;
			pr_info(DEVICE_NAME ": Config: %s\n", platform_name);
		}

		kfree(platform_name);
	}

	pr_warn(DEVICE_NAME " probe complete");

	return 0;
}

static int voxl_platform_remove(struct platform_device *pdev)
{
	pr_warn(DEVICE_NAME " remove\n");
	device_destroy(voxl_platform_class, MKDEV(voxl_platform_dev_major, 0));
	class_destroy(voxl_platform_class);
	unregister_chrdev(voxl_platform_dev_major, DEVICE_NAME);
	return 0;
}

static const struct of_device_id of_voxl_platform_dt_match[] = {
	{.compatible	= "modalai,voxl-platform"},
	{},
};

MODULE_DEVICE_TABLE(of, of_voxl_platform_dt_match);

static struct platform_driver voxl_platform_driver = {
	.probe	= voxl_platform_probe,
	.remove	= voxl_platform_remove,
	.driver	= {
		.name	= "voxl_platform",
		.of_match_table	= of_voxl_platform_dt_match,
	},
};

static int __init voxl_platform_mod_init(void)
{
	pr_warn(DEVICE_NAME " init\n");
	return platform_driver_register(&voxl_platform_driver);
}

static void __exit voxl_platform_mod_exit(void)
{
	pr_warn(DEVICE_NAME " exit\n");
	platform_driver_unregister(&voxl_platform_driver);
}

module_init(voxl_platform_mod_init);
module_exit(voxl_platform_mod_exit);

MODULE_DESCRIPTION("VOXL Platform initialization module");
MODULE_AUTHOR("Travis Bottalico <travis@modalai.com>");
MODULE_VERSION(DRIVER_VERSION);
MODULE_LICENSE("GPL");